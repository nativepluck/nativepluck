class Genericmodel < ApplicationRecord
  include Nativepluck
  has_many :another_generic_models, dependent: :destroy
end
