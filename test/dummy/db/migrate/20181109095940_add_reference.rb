class AddReference < ActiveRecord::Migration[5.2]
  def change
    add_reference :another_generic_models, :genericmodel , index: true, foreign_key: true

  end
end
