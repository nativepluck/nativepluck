class CreateAnotherGenericModels < ActiveRecord::Migration[5.2]
  def change
    create_table :another_generic_models do |t|
      t.integer  :another_integer_col
      t.string   :another_string_col
      t.datetime :another_datetime_col
      t.float    :another_float_col
      t.json     :another_json_col
      t.jsonb    :another_jsonb_col
      t.timestamps
    end
  end
end
