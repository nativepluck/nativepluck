class CreateGenericmodels < ActiveRecord::Migration[5.2]
  def change
    create_table :genericmodels do |t|
      t.integer  :integer_col
      t.string   :string_col
      t.datetime :datetime_col
      t.float    :float_col
      t.json     :json_col
      t.jsonb    :jsonb_col
      t.timestamps
    end
  end
end
