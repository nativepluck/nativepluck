def rand_str(size=26)
  (0...13).map { ('a'..'z').to_a[rand(size)] }.join
end
genericmodels = []

1000.times do
  json = {
      id: rand(100_00),
      string: rand_str,
      datetime: DateTime.now,
      float: rand(100_000) + rand()
  }
  2.times do
    # To make order by tests cover more cases
    genericmodels << Genericmodel.new(
        integer_col:  rand(100_000),
        float_col:    rand(100_000) + rand(),
        string_col:   rand_str,
        datetime_col: DateTime.now,
        json_col:     json,
        jsonb_col:    json
    )
  end
end

Genericmodel.import genericmodels

another_generic_models = []
genericmodel_ids = Genericmodel.pluck(:id)

1000.times do
  genericmodel_id = genericmodel_ids.sample(1).first
  json = {
      id: rand(100_00),
      string: rand_str,
      datetime: DateTime.now,
      float: rand(100_000) + rand()
  }
  another_generic_models << AnotherGenericModel.new(
      another_integer_col:  rand(100_000),
      another_float_col:    rand(100_000) + rand(),
      another_string_col:   rand_str,
      another_datetime_col: DateTime.now,
      another_json_col:     json,
      another_jsonb_col:    json,
      genericmodel_id: genericmodel_id
  )
end

AnotherGenericModel.import another_generic_models

