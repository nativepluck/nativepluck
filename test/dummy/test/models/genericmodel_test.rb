require 'test_helper'
#
# We use order in all tests to prevent tests from failing due to DB fetching rows in a different order
#
METHODS = [:pluck, :nativepluck]
class GenericmodelTest < ActiveSupport::TestCase
  ##########################################################
  test 'pluck_each_attribute' do
    failed = []
    Genericmodel.attribute_names.each do |attr_name|
      results = []
      METHODS.each do |method_name|
        results << Genericmodel.order(id: :asc).send(method_name, attr_name)
      end
      failed << attr_name if results[0] != results[1]
    end
    assert(failed.empty?, "#{__method__}:: pluck != nativepluck for attributes : #{failed.join(', ')}")
  end
  ##########################################################
  test 'pluck_permutations' do
    row_limit = 100
    failed = []
    Genericmodel.attribute_names.permutation(2).each do |attrs|
      results = []
      METHODS.each do |method_name|
        results << Genericmodel.limit(row_limit).order(id: :asc).send(method_name, *attrs)
      end
      failed << attrs if results[0] != results[1]
    end
    assert(failed.empty?, "#{__method__}:: pluck != nativepluck for attributes : #{failed.join(', ')}")
  end
  ##########################################################
  test 'test_limit' do
    Range.new(1,100).step(rand(5) + 1).each do |limit|
      results = []
      METHODS.each do |method_name|
        results << Genericmodel.limit(limit).order(id: :asc).send(method_name, :id)
      end
      assert(results[0] == results[1], "#{__method__}:: pluck != nativepluck for column :id and limit #{limit}")
    end
  end
  ##########################################################
  test 'test_offset' do
    row_limit = 100
    Range.new(1,100).step(rand(5) + 1).each do |offset|
      results = []
      METHODS.each do |method_name|
        results << Genericmodel.limit(row_limit).offset(offset).order(id: :asc).send(method_name, :id)
      end
      assert(results[0] == results[1], "#{__method__}:: pluck != nativepluck for column :id and offset #{offset}")
    end
  end
  ##########################################################
  test 'test_order' do
    failed = []
    Genericmodel.attribute_names.each do |attr_name|
      next if attr_name.match(/json/) # Cannot order by JSON(B) column
      results = []
      METHODS.each do |method_name|
        results << Genericmodel.order(attr_name => :asc, :id => :asc).send(method_name, attr_name)
      end
      failed << attr_name if results[0] != results[1]
    end
    assert(failed.empty?, "#{__method__}:: pluck != nativepluck for attributes : #{failed.join(', ')}")
  end
  ##########################################################
  test 'test_group_by' do
    row_limit = 100
    results = []
    METHODS.each do |method_name|
      results << Genericmodel.limit(row_limit).order(id: :asc).group(:id).send(method_name, :id)
    end
    assert(results[0] == results[1], "#{__method__}:: pluck != nativepluck")
  end
  ##########################################################
  test 'Nativepluck_nativepluck_query' do
    row_limit = 100
    failed = []
    Genericmodel.attribute_names.each do |attr_name|
      pluck = Genericmodel.limit(row_limit).order(id: :asc).pluck(attr_name)
      nativepluck  = Nativepluck.nativepluck(Genericmodel.limit(row_limit).order(id: :asc).select(attr_name))
      failed << attr_name if pluck != nativepluck
    end
    assert(failed.empty?, "#{__method__}:: pluck != nativepluck for attributes : #{failed.join(', ')}")
  end
  ##########################################################
  test 'Nativepluck_nativepluck_string' do
    row_limit = 100
    failed = []
    Genericmodel.attribute_names.each do |attr_name|
      pluck = Genericmodel.limit(row_limit).order(id: :asc).pluck(attr_name)
      nativepluck  = Nativepluck.nativepluck(Genericmodel.limit(row_limit).order(id: :asc).select(attr_name).to_sql)
      failed << attr_name if pluck != nativepluck
    end
    assert(failed.empty?, "#{__method__}:: pluck != nativepluck for attributes : #{failed.join(', ')}")
  end
  ##########################################################
end
