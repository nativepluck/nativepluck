require 'memory_profiler'
require 'benchmark/ips'
require 'pg'
#
# We use order in all tests to prevent tests from failing due to DB fetching rows in a different order
#
module BenchMarks
  module RunTime
    def self.benchmark
      loggers    = [ActiveRecord::Base.logger, ActiveModelSerializers.logger]
      ActiveRecord::Base.logger ,ActiveModelSerializers.logger = nil , Logger.new(nil)
      limit = Genericmodel.count
      results = {}
      Genericmodel.attribute_names.each do |attribute_name|
        sql_raw = Genericmodel.order(id: :asc).limit(limit).select(attribute_name).to_sql
        results[attribute_name] = Benchmark.ips do |x|
          x.config(time: 5, warmup: 1)
          x.report("pluck")                  { Genericmodel.order(id: :asc).limit(limit).pluck(attribute_name) }
          x.report("nativepluck")            { Genericmodel.order(id: :asc).limit(limit).nativepluck(attribute_name) }
          x.report("nativepluck_raw_to_sql") { Nativepluck.nativepluck(Genericmodel.order(id: :asc).limit(limit).select(attribute_name)) }
          x.report("nativepluck_raw_SQL")    { Nativepluck.nativepluck(sql_raw) }
          x.compare!
        end
      end
      ActiveRecord::Base.logger , ActiveModelSerializers.logger = loggers ; nil
      File.open('runtime.csv','w') do |fh|
        fh.puts("Attribute Name, pluck[iter/sec], nativepluck[iter/sec], nativepluck_raw_to_sql[iter/sec], nativepluck_raw_SQL[iter/sec]")
        results.each_pair do |attribute_name, report|
          str = "#{attribute_name}"
          report.entries.each do |entry|
            str << ",#{entry.stats.instance_variable_get(:@mean).to_f.round(1)}"
          end
          fh.puts(str)
        end
      end
    end
  end
end
