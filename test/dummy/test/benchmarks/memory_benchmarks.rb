require 'memory_profiler'
require 'benchmark/ips'
require 'pg'
module BenchMarks
  module Memory
    def self.benchmark(write_to_file: true, row_limit: nil)
      limit = row_limit || Genericmodel.count
      results = nil ; # Used to prevent the GC from collecting the output
      reports = {pluck: {}, nativepluck: {}, nativepluck_raw_to_sql: {}, nativepluck_raw_string: {}}
      Genericmodel.attribute_names.each do |attribute_name|
        ##################################################################################################
        Genericmodel.limit(limit).order(id: :desc).pluck(attribute_name)
        reports[:pluck][attribute_name] = MemoryProfiler.report do
          results = Genericmodel.limit(limit).order(id: :desc).pluck(attribute_name)
        end
        ##################################################################################################
        Genericmodel.limit(limit).order(id: :desc).nativepluck(attribute_name)
        reports[:nativepluck][attribute_name] = MemoryProfiler.report do
          results = Genericmodel.limit(limit).order(id: :desc).nativepluck(attribute_name)
        end
        ##################################################################################################
        Nativepluck.nativepluck(Genericmodel.limit(limit).order(id: :desc).select(attribute_name))
        reports[:nativepluck_raw_to_sql][attribute_name] = MemoryProfiler.report do
          results = Nativepluck.nativepluck(Genericmodel.limit(limit).order(id: :desc).select(attribute_name))
        end
        ##################################################################################################
        sql = Genericmodel.limit(limit).order(id: :desc).select(attribute_name).to_sql
        Nativepluck.nativepluck(sql)
        reports[:nativepluck_raw_string][attribute_name] = MemoryProfiler.report do
          results = Nativepluck.nativepluck(sql)
        end
        ##################################################################################################
      end
      ##################################################################################################
      return unless write_to_file
      text_report = {}
      reports.each_pair do |type, results_per_attribute|
        results_per_attribute.each_pair do |attribute_name, result|
          text_report[attribute_name] ||= []
          text_report[attribute_name] << [type, attribute_name, result.total_allocated, result.total_retained]
        end
      end ; nil

      ##################################################################################################
      # CSV report format:
      # Method (pluck/nativepluck/etc) , Attribute Name, Total Allocated , Total Retained
      File.open('memory_report.csv','w') do |fh|
        fh.puts("Number of rows : #{limit}")
        fh.puts('Method, Attribute Name, Total Allocated, Total Retained')
        text_report.each_value do |results|
          results.each {|i| fh.puts(i.join(',')) }
        end
      end
      ##################################################################################################
      all_allocated_memory = {}
      all_allocated_objects = {}
      all_methods = {}
      by_attributes_memory = {}
      by_attributes_objects = {}
      reports.each_pair do |type, results_per_attribute|
        results_per_attribute.each_pair do |attribute_name, result|
          result.allocated_memory_by_class.each do |i|
            all_methods[type] ||= type
            all_allocated_memory[i[:data]] ||= {}
            all_allocated_memory[i[:data]][attribute_name] ||= {}
            all_allocated_memory[i[:data]][attribute_name][type] ||= 0
            all_allocated_memory[i[:data]][attribute_name][type] += i[:count].to_i
            by_attributes_memory[attribute_name] ||= {}
            by_attributes_memory[attribute_name][i[:data]] ||= {}
            by_attributes_memory[attribute_name][i[:data]][type] ||= 0
            by_attributes_memory[attribute_name][i[:data]][type] += i[:count].to_i
          end
          result.allocated_objects_by_class.each do |i|
            all_methods[type] ||= type
            all_allocated_objects[i[:data]] ||= {}
            all_allocated_objects[i[:data]][attribute_name] ||= {}
            all_allocated_objects[i[:data]][attribute_name][type] ||= 0
            all_allocated_objects[i[:data]][attribute_name][type] += i[:count].to_i
            by_attributes_objects[attribute_name] ||= {}
            by_attributes_objects[attribute_name][i[:data]] ||= {}
            by_attributes_objects[attribute_name][i[:data]][type] ||= 0
            by_attributes_objects[attribute_name][i[:data]][type] += i[:count].to_i
          end
        end
      end
      ##################################################################################################
      # CSV report format :
      # Attribute Name, Class , Method1 , Method2 ... MethodN
      File.open('by_attributes_objects.csv','w') do |fh|
        fh.puts("Number of rows : #{limit}")
        str = 'Attribute Name, Class'
        all_methods.each_key {|type| str << ",#{type}"}
        fh.puts(str)
        by_attributes_objects.each_pair do |attribute_name, results_by_class|
          results_by_class.each_pair do |class_name, results_per_type|
            str = "#{attribute_name},#{class_name}"
            all_methods.each_key do |type|
              str << ",#{results_per_type.fetch(type,0)}"
            end
            fh.puts(str)
          end
        end
      end
      ##################################################################################################
      # CSV report format :
      # Attribute Name, Class , Method1 , Method2 ... MethodN
      File.open('by_attributes_memory.csv','w') do |fh|
        fh.puts("Number of rows : #{limit}")
        str = 'Attribute Name, Class'
        all_methods.each_key {|type| str << ",#{type}"}
        fh.puts(str)
        by_attributes_memory.each_pair do |attribute_name, results_by_class|
          results_by_class.each_pair do |class_name, results_per_type|
            str = "#{attribute_name},#{class_name}"
            all_methods.each_key do |type|
              str << ",#{results_per_type.fetch(type,0)}"
            end
            fh.puts(str)
          end
        end
      end
      ##################################################################################################
      # CSV report format :
      # Class, Attribute Name , Method1 , Method2 ... MethodN
      File.open('allocated_objects_by_class.csv','w') do |fh|
        fh.puts("Number of rows : #{limit}")
        str = 'Class, Attribute Name'
        all_methods.each_key {|i| str << ", #{i}" }
        fh.puts(str)
        all_allocated_objects.each_pair do |klass, results_per_attribute|
          results_per_attribute.each_pair do |attribute_name, results_per_type|
            str = "#{klass}, #{attribute_name}"
            all_methods.each_key do |type|
              str << ",#{results_per_type.fetch(type,0)}"
            end
            fh.puts(str)
          end
        end
      end
      ##################################################################################################
      # CSV report format:
      # Class, Attribute Name , Method1 , Method2 ... MethodN
      File.open('allocated_memory_by_class.csv','w') do |fh|
        fh.puts("Number of rows : #{limit}")
        str = 'Class, Attribute Name'
        all_methods.each_key {|i| str << ", #{i}" }
        fh.puts(str)
        all_allocated_memory.each_pair do |klass, results_per_attribute|
          results_per_attribute.each_pair do |attribute_name, results_per_type|
            str = "#{klass}, #{attribute_name}"
            all_methods.each_key do |type|
              str << ",#{results_per_type.fetch(type,0)}"
            end
            fh.puts(str)
          end
        end
      end
      ##################################################################################################
    end
    ##################################################################################################
  end
end

