# Copyright 2018 Ohad Dahan, Al Chou
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

require "nativepluck/version"

module Nativepluck
  @override_pluck = @nativepluck_init = false

  class << self
    attr_accessor :nativepluck_type_map_for_results, :nativepluck_type_map_for_queries
    attr_accessor :original_type_map_for_results, :original_type_map_for_queries
    attr_accessor :nativepluck_init, :override_pluck
  end



  def self.init_nativepluck
    return if @nativepluck_init
    @nativepluck_type_map_for_results = PG::BasicTypeMapForResults.new ActiveRecord::Base.connection.raw_connection
    @nativepluck_type_map_for_queries = PG::BasicTypeMapForQueries.new ActiveRecord::Base.connection.raw_connection
    @original_type_map_for_results    = ActiveRecord::Base.connection.raw_connection.type_map_for_results
    @original_type_map_for_queries    = ActiveRecord::Base.connection.raw_connection.type_map_for_queries
    @nativepluck_init = true
  end

  def self.set_pg_native_casters
    init_nativepluck
    ActiveRecord::Base.connection.raw_connection.type_map_for_results = Nativepluck.nativepluck_type_map_for_results
    ActiveRecord::Base.connection.raw_connection.type_map_for_queries = Nativepluck.nativepluck_type_map_for_queries
  end

  def self.return_original_casters
    init_nativepluck
    ActiveRecord::Base.connection.raw_connection.type_map_for_results = Nativepluck.original_type_map_for_results
    ActiveRecord::Base.connection.raw_connection.type_map_for_queries = Nativepluck.original_type_map_for_queries
  end

  def self.nativepluck(input)
    out = []
    begin
      sql = input.respond_to?(:to_sql) ? input.to_sql : input
      Nativepluck.set_pg_native_casters
      results = ActiveRecord::Base.connection.raw_connection.async_exec(sql)
      results.nfields == 1 ? out = results.column_values(0) : out = results.values
    ensure
      results.try(:clear)
      Nativepluck.return_original_casters
    end
    return out
  end

  module InstanceMethods
    def nativepluck(*column_names)
      # Extracted (before modifications) from:
      # ruby-2.5.0/gems/activerecord-5.2.1/lib/active_record/relation/calculations.rb
      if loaded? && (column_names.map(&:to_s) - @klass.attribute_names - @klass.attribute_aliases.keys).empty?
        return records.nativepluck(*column_names)
      end

      if has_include?(column_names.first)
        relation = apply_join_dependency
        relation.nativepluck(*column_names)
      else
        enforce_raw_sql_whitelist(column_names)
        relation = spawn
        relation.select_values = column_names.map { |cn|
          @klass.has_attribute?(cn) || @klass.attribute_alias?(cn) ? arel_attribute(cn) : cn
        }
        Nativepluck.nativepluck(relation)
      end
    end
  end

  module ::ActiveRecord
    module Calculations
      include Nativepluck::InstanceMethods
    end
    module Querying
      delegate :nativepluck, to: :all
    end
  end

  def self.set_override_pluck(selection)
    raise ArgumentError.new("#{__method__}:: Input should be true/false and not #{selection.class}") unless (selection.is_a?(TrueClass) || selection.is_a?(FalseClass))

    if selection
      ::ActiveRecord::Calculations.class_eval do
        alias_method :orig_pluck, :pluck
        alias_method :pluck, :nativepluck
      end
      ::ActiveRecord::Querying.class_eval do
        alias_method :orig_pluck, :pluck
        alias_method :pluck, :nativepluck
      end
    else
      unless Nativepluck.override_pluck
        raise ArgumentError.new("#{__method__}:: Cannot turn off pluck overriding since it wasn't set yet")
      end
      ::ActiveRecord::Calculations.class_eval do
        alias_method :pluck, :orig_pluck
      end
      ::ActiveRecord::Querying.class_eval do
        alias_method :pluck, :orig_pluck
      end
    end
    Nativepluck.override_pluck = selection
  end
end
