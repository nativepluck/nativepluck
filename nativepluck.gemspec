lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'nativepluck/version'

Gem::Specification.new do |spec|
  spec.name        = 'nativepluck'
  spec.version     = Nativepluck::VERSION
  spec.authors     = ['Ohad Dahan', 'Al Chou']
  spec.email       = ['HotFusionMan+GitLab-Nativeson@Gmail.com']
  spec.summary     = 'nativepluck'
  spec.description = 'An alternative implementation of pluck, based on native database casting'
  spec.homepage    = 'https://gitlab.com/Nativepluck/nativepluck'
  spec.license     = 'Apache-2.0'
  spec.files = Dir['{app,config,db,lib}/**/*', 'LICENSE', 'README.md']
  spec.add_dependency 'rails', '>= 5.2.1'
  spec.add_dependency 'pg'
end
