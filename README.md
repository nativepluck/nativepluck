# Nativepluck

Nativepluck is a high-performance alternative to ActiveRecord's `pluck` when using PostgreSQL.
Via native PostgreSQL data type casting, we achieve significant memory and time savings.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'nativepluck'
```
And then execute:
```
    $ bundle
```

Or install it yourself as:
```
    $ gem install nativepluck
```

## Usage

* You can use `nativepluck` just like you use `pluck` , as in `<Model>.limit(10).nativepluck(:id,:name)`<br>
* You can also set in your `config/initializers/nativepluck.rb` , `Nativepluck.set_override_pluck true` and we'll override `pluck` with `nativepluck`<br>
  No changes needed on your side, completely transparent<br>
    
* You can take an existing ActiveRecord query method call chain, for example:<br>
  `<Model>.where('id >= 21').limit(100).joins(:some_association_to_another_model).pluck(:id)`<br>
  Change the `pluck` call to `select` and use it as the argument to the `nativepluck` module method:<br>
   `Nativepluck.nativepluck(<Model>.where('id >= 21').limit(100).joins(:some_association_to_another_model).select(:id))`<br>
  Internally, this method calls `to_sql` on the supplied `ActiveRecord::Relation` instance and submits the generated SQL query to the database.  Again, native PostgreSQL typecasting is performed on the query results.<br>
  
* You can also use a raw SQL string argument in the module method, e.g.,<br>
  `Nativepluck.nativepluck('SELECT models.id FROM models JOIN some_other_models ON some_other_models.model_id = models.id WHERE models.id > 21 LIMIT 100')`.
  
## Benchmarks

Due to GitLab not rendering CSV properly we also host the CSV results at [GitHub](https://github.com/ohaddahan/csv_depo/tree/master)<br>

## Memory Benchmarks
 
To run the benchmarks use:
`BenchMarks::Memory::benchmark(write_to_file: true|false, row_limit: nil|integer)`<br>

* We're seeing up to ~50X less object allocations for integer columns<br>

### Memory allocation benchmark 
* Benchmark code `test/dummy/test/benchmarks/memory_benchmarks.rb`
* Benchmark CSV `test/dummy/memory_report.csv`

### Allocated Objects by Class Benchmark

* Benchmark code `test/dummy/test/benchmarks/memory_benchmarks.rb`
* Benchmark CSV by objects `test/dummy/by_attributes_objects.csv`
* Benchmark CSV by memory `test/dummy/by_attributes_memory.csv`


## RunTime Benchmarks
 
To run the benchmarks use:
`BenchMarks::RunTime::benchmark`<br>

* Benchmark code `test/dummy/test/benchmarks/runtime_benchmarks.rb`<br>
* We're seeing up to ~4X faster run-time when plucking DateTime columns and ~1.5X on integer columns
* Full results at `test/dummy/test/benchmarks/runtime.csv` 


## License

The gem is available as open source under the terms of the [Apache 2.0 License](https://opensource.org/licenses/Apache-2.0).

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org/).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/nativepluck/nativepluck. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org/) code of conduct.

## Code of Conduct

Everyone interacting in the Nativepluck project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/nativepluck/nativepluck/blob/master/CODE_OF_CONDUCT.md).
